require 'gem2deb/rake/testtask'

['adsf', 'adsf-live'].each do |pkg|
  task "test-#{pkg}" do
    Dir.chdir(pkg) do
      ruby '-S', 'rake', 'test'
    end
  end
  task default: "test-#{pkg}"
end
